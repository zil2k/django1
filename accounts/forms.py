from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, UserChangeForm
from accounts.models import Account


class LoginForm(AuthenticationForm):
    class Meta:
        model = Account
        fields = ('username', 'password')

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class RegisterForm(UserCreationForm):
    class Meta:
        model = Account
        fields = ('username', 'first_name', 'last_name', 'password1', 'password2', 'email', 'age', 'avatar', 'phone')

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            field.help_text = ''

    def clean_age(self):
        data = self.cleaned_data['age']
        if data and data < 18:
            raise forms.ValidationError("Вы слишком молоды!")
        return data


class UserEditForm(UserChangeForm):
    class Meta:
        model = Account
        fields = ('password', 'first_name', 'last_name', 'email', 'age', 'avatar')
    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            field.help_text = ''
            if field_name == 'password':
               field.widget = forms.PasswordInput()

    def clean_age(self):
        data = self.cleaned_data['age']
        if data < 18:
            raise forms.ValidationError("Вы слишком молоды!")

        return data