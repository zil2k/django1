from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Register your models here.

from accounts.models import Account

from django.template.loader import render_to_string


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        'username', 'picture', 'group', 'first_name', 'last_name',
        'email', 'age', 'phone'
    ]

    search_fields = [
        'username', 'email', 'phone'
    ]

    def picture(self, obj):
        if obj.avatar is not None:
            return render_to_string(
                'server/picture.html',
                {'image': obj.avatar.url, 'name': obj.avatar.name}
            )
        return render_to_string(
            'server/picture.html',
            {'image': None, 'name': 'image not found'}
        )

    def group(self, obj):
        return [x for x in obj.groups.all()]
