from django.urls import path
from accounts.views import login_user, edit_user, logout, RegisterUser

app_name = 'accounts'

urlpatterns = [
    path('login/', login_user, name='login'),
    path('register/', RegisterUser.as_view(), name='register'),
    path('edit/', edit_user, name='edit'),
    path('logout/', logout, name='logout'),
]
