from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib import auth
from accounts.forms import LoginForm, RegisterForm, UserEditForm
from accounts.models import Account
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView


def login_user(request):
    title = 'Вход'
    success_url = 'catalog:catalog'
    login_form = LoginForm(data=request.POST)
    if request.method == 'POST':
        user = request.POST.get('username')
        passwd = request.POST.get('password')
        auth_user = auth.authenticate(username=user, password=passwd)

        if auth_user and auth_user.is_active:
            auth.login(request, auth_user)
            return redirect(success_url)

    content = {'title': title, 'login_form': login_form}

    return render(request, 'accounts/login.html', content)


class RegisterUser(CreateView):
    model = Account
    template_name = 'accounts/register.html'
    form_class = RegisterForm
    success_url = reverse_lazy('accounts:login')


@login_required(login_url=reverse_lazy('accounts:login'))
def edit_user(request):
    title = 'редактирование'

    if request.method == 'POST':
        edit_form = UserEditForm(request.POST, request.FILES, instance=request.user)
        if edit_form.is_valid():
            edit_form.save()
            success_url = reverse_lazy('accounts:login')
            return redirect(success_url)
    else:
        edit_form = UserEditForm(instance=request.user)

    content = {'title': title, 'edit_form': edit_form}

    return render(request, 'accounts/edit.html', content)


@login_required(login_url=reverse_lazy('accounts:login'))
def logout(request):
    auth.logout(request)
    success_url = reverse_lazy('accounts:login')
    return redirect(success_url)
