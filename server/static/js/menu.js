const CatalogCategory = ({pk, name}) => (
    `<li class="submenu">
            <a href="/catalog/category/${pk}/">${name}</a>
    </li>`
);

const renderData = res => {
    menuHtml = res.data.map(CatalogCategory)
        .join('');
    menu = document.getElementById('categories', 'pk');
    menu.innerHTML += menuHtml;
};