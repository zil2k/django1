from django.urls import path
from catalog.views import (
    CatalogView, ProductDeleteView, ProductUpdateView,
    ProductDetailView, ProductCreateView
    )

from catalog.views import (
    CatalogView, ProductDeleteView, ProductUpdateView,
    ProductDetailView, ProductCreateView, CategoryView
    )

app_name = 'catalog'


urlpatterns = [
    path('catalog/', CatalogView.as_view(), name='catalog'),
    path('catalog/category/<int:pk>/', CategoryView.as_view(), name='category'),
    path('create/', ProductCreateView.as_view(), name='create'),
    path('catalog/update/<int:pk>/', ProductUpdateView.as_view(), name='update'),
    path('catalog/delete/<int:pk>/', ProductDeleteView.as_view(), name='delete'),
    path('catalog/detail/<int:pk>/', ProductDetailView.as_view(), name='detail'),

]
