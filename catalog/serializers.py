from rest_framework import serializers
from rest_framework.response import Response
from catalog.models import CatalogCategory, ProductDetail


class CatalogSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatalogCategory
        fields = [
            'name', 'description', 'modified',
            'created', 'pk'
        ]


class ProductSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField('get_image_url')

    def get_image_url(self, obj):
        return obj.image.value.url

    class Meta:
        model = ProductDetail
        fields = [
            'name', 'image', 'short_desc',
            'price', 'pk',
        ]
