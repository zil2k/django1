from catalog.models import CatalogCategory
from rest_framework.viewsets import ModelViewSet
from catalog.serializers import CatalogSerializer


class CatalogViewSet(ModelViewSet):
    queryset = CatalogCategory.objects.all()
    serializer_class = CatalogSerializer
