from catalog.models import ProductDetail
from rest_framework.viewsets import ModelViewSet
from catalog.serializers import ProductSerializer
from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 6
    page_size_query_param = 'page_size'
    max_page_size = 6


class ProductViewSet(ModelViewSet):
    queryset = ProductDetail.objects.all()
    serializer_class = ProductSerializer
    pagination_class = StandardResultsSetPagination
