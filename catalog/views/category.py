from django.views.generic import (
    ListView, CreateView, UpdateView,
    DeleteView
)

from django.urls import reverse_lazy

from catalog.forms import CategoryForm
from catalog.models import ProductDetail, CatalogCategory
from accounts.mixins import AdminGroupRequired
from django.contrib.auth.mixins import LoginRequiredMixin



class CatalogView(ListView):
    model = ProductDetail
    queryset = CatalogCategory.objects.all()
    template_name = 'catalog/catalog.html'
    context_object_name = 'results'
    paginate_by = 3


class CategoryView(ListView):
    model = ProductDetail
    template_name = 'catalog/category.html'
    context_object_name = 'results'
    paginate_by = 3

    def get_queryset(self, **kwargs):
        return ProductDetail.objects.filter(
            category_id=self.kwargs['pk']
        )


class CategoryCreateView(LoginRequiredMixin, AdminGroupRequired, CreateView):
    model = CatalogCategory
    template_name = 'catalog/create.html'
    form_class = CategoryForm
    success_url = reverse_lazy('catalog:catalog')
    redirect_url = success_url


class CategoryUpdateView(LoginRequiredMixin, AdminGroupRequired, UpdateView):
    model = CatalogCategory
    template_name = 'catalog/create.html'
    form_class = CategoryForm
    success_url = reverse_lazy('catalog:catalog')
    redirect_url = success_url


class CategoryDeleteView(LoginRequiredMixin, AdminGroupRequired, DeleteView):
    model = CatalogCategory
    template_name = 'catalog/delete.html'
    success_url = reverse_lazy('catalog:catalog')
    redirect_url = success_url
