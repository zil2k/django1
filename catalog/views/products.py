from django.views.generic import (
    DetailView, CreateView,UpdateView,
    DeleteView
)

from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from catalog.forms import ProductForm
from catalog.models import ProductDetail
from accounts.mixins import AdminGroupRequired


class ProductDetailView(DetailView):
    model = ProductDetail
    template_name = 'catalog/detail.html'
    context_object_name = 'instance'


class ProductCreateView(LoginRequiredMixin, AdminGroupRequired, CreateView):
    model = ProductDetail
    template_name = 'catalog/create.html'
    form_class = ProductForm
    success_url = reverse_lazy('catalog:catalog')
    redirect_url = success_url
    login_url = reverse_lazy('accounts:login')


class ProductUpdateView(LoginRequiredMixin, AdminGroupRequired, UpdateView):
    model = ProductDetail
    template_name = 'catalog/create.html'
    form_class = ProductForm
    success_url = reverse_lazy('catalog:catalog')
    redirect_url = success_url
    login_url = reverse_lazy('accounts:login')


class ProductDeleteView(LoginRequiredMixin, AdminGroupRequired, DeleteView):
    model = ProductDetail
    template_name = 'catalog/delete.html'
    success_url = reverse_lazy('catalog:catalog')
    redirect_url = success_url
    login_url = reverse_lazy('accounts:login')
