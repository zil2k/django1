from django import forms
from catalog.models import CatalogCategory, ProductDetail



class CategoryForm(forms.ModelForm):
    class Meta:
        model = CatalogCategory
        fields = ('name', 'description')

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'default_form'



class ProductForm(forms.ModelForm):
    class Meta:
        model = ProductDetail
        fields = (
            'name',
            'category',
            'image',
            'description',
            'short_desc',
            'spec',
            'price',
            'quantity'
        )

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'default_form'

